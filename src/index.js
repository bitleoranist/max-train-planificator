require('dotenv').config();

const BootBot = require('bootbot');
const moment = require('moment');
const app = require('./app');
const requests = require('./requests')(app);

const AlertModel = require('./models/alert');

const { logger } = app;

const fbBot = new BootBot({
  accessToken: app.config.get('fb.accessToken'),
  verifyToken: app.config.get('fb.verifyToken'),
  appSecret: app.config.get('fb.appSecret'),
});

fbBot.deletePersistentMenu();
fbBot.setGetStartedButton('Start saving time');
fbBot.setGreetingText('I am here to help you never miss TGVMax tickets. If you want to try me and test me, do not hesitate, it\'s allowed ;)');

fbBot.hear(['hello', 'hi'], async (_payload, chat) => {
  const user = await chat.getUserProfile();
  chat.say(`Nice to meet you, ${user.first_name}!`);
});

fbBot.hear(['book', 'alert'], async (_payload, chat) => {
  const token = await requests.tgvMaxSimulatorApiToken();
  logger.debug(`Fresh token received "${token}"`);

  async function isCancelled(payload, conv) {
    if (payload.message.text.trim().toLowerCase().includes('cancel')) { // trimed and standardized
      await conv.say('Ok!');
      conv.end();
      return true;
    }
    return false;
  }

  async function registerAlert(conv, origin, destination, dateStart, dateEnd) {
    const userProfile = await conv.getUserProfile();

    // eslint-disable-next-line no-new
    AlertModel.create({
      fbUserId: userProfile.id,
      trainOrigin: {
        date: dateStart.toDate(),
        station: origin,
      },
      trainDestination: {
        date: dateEnd.toDate(),
        station: destination,
      },
    }, async (err, alertModel) => {
      if (err) {
        logger.error(err);
        return;
      }

      logger.debug('Alert "%s" created', alertModel.id);

      await conv.say('Done! You\'ll be warned when a free seat become available :)');
      conv.end();
    });
  }

  const askDateEnd = async (_conv) => {
    _conv.ask('What date and what time do you want to go to, maximum?', async (payload, conv) => {
      if (await isCancelled(payload, conv)) return; // test if cancellation demand

      const dateEndText = payload.message.text.trim().toLowerCase(); // trim and standardize

      // const userProfile = await conv.getUserProfile();
      // moment.utcOffset(userProfile.timezone)

      const dateEndDate = moment(dateEndText);
      if (!dateEndDate.isValid()) {
        await conv.say('I can\'t recognize this dateTime, please retry?');
        askDateEnd(conv);
        return;
      }

      await conv.say(`Oh, ${dateEndDate.format()}, well noted`);
      conv.set('dateEnd', dateEndDate);

      registerAlert(conv, conv.get('origin'), conv.get('destination'), conv.get('dateStart'), conv.get('dateEnd'));
    });
  };

  const askDateStart = async (_conv) => {
    _conv.ask('When and what day would you like to leave, minimum?', async (payload, conv) => {
      if (await isCancelled(payload, conv)) return; // test if cancellation demand

      const dateStartText = payload.message.text.trim().toLowerCase(); // trim and standardize

      // const userProfile = await conv.getUserProfile();
      // moment.utcOffset(userProfile.timezone)

      const dateStartDate = moment(dateStartText);
      if (!dateStartDate.isValid()) {
        await conv.say('I can\'t recognize this dateTime, please retry?');
        askDateStart(conv);
        return;
      }

      await conv.say(`Oh, ${dateStartDate.format()}, well noted`);
      conv.set('dateStart', dateStartDate);
      askDateEnd(conv);
    });
  };

  const askDestination = async (_conv) => {
    const destinationFirstLetters = _conv.get('destinationFirstLetters');
    const selectedOrigin = _conv.get('origin');
    const destinations = await requests.tgvMaxSimulatorApiDestinationsByOrigin(token, selectedOrigin);
    const selectedDestinations = [];

    destinations.forEach((destination) => {
      if (destination.toLowerCase().startsWith(destinationFirstLetters)) {
        selectedDestinations.push(destination);
      }
    });

    logger.debug(selectedDestinations);

    const question = {
      text: 'Where do you want to go then?',
      quickReplies: selectedDestinations,
    };

    _conv.ask(question, async (payload, conv) => {
      if (await isCancelled(payload, conv)) return; // test if cancellation demand

      const destination = payload.message.text;
      await conv.say(`Oh, ${destination}, well noted`);
      conv.set('destinations', destination);
      askDateStart(conv);
    });
  };

  const askDestinationFirstLetters = async (_conv) => {
    _conv.ask('Please type the first letters of the destination.', async (payload, conv) => {
      if (await isCancelled(payload, conv)) return; // test if cancellation demand

      const originFirstLetters = payload.message.text.trim().toLowerCase(); // trim and standardize
      await conv.say(`Oh, ${originFirstLetters}, well noted`);
      conv.set('destinationFirstLetters', originFirstLetters);
      askDestination(conv);
    });
  };

  const askOrigin = async (_conv) => {
    const originFirstLetters = _conv.get('originFirstLetters');
    const origins = await requests.tgvMaxSimulatorApiOriginsAll(token);
    const selectedOrigins = [];

    origins.forEach((origin) => {
      if (origin.toLowerCase().startsWith(originFirstLetters)) {
        selectedOrigins.push(origin);
      }
    });

    logger.debug(selectedOrigins);

    const question = {
      text: 'From which origin do you want to add an alert?',
      quickReplies: selectedOrigins,
    };

    _conv.ask(question, async (payload, conv) => {
      if (await isCancelled(payload, conv)) return; // test if cancellation demand

      const origin = payload.message.text;
      await conv.say(`Oh, ${origin}, well noted`);
      conv.set('origin', origin);
      askDestinationFirstLetters(conv);
    });
  };

  const askOriginFirstLetters = async (_conv) => {
    _conv.ask('Please type the first letters of the origin.', async (payload, conv) => {
      if (await isCancelled(payload, conv)) return; // test if cancellation demand

      const originFirstLetters = payload.message.text.trim().toLowerCase(); // trim and standardize
      await conv.say(`Oh, ${originFirstLetters}, well noted`);
      conv.set('originFirstLetters', originFirstLetters);
      askOrigin(conv);
    });
  };

  chat.conversation(async (conv) => {
    askOriginFirstLetters(conv);
  });
});

fbBot.start(app.config.get('server.port'));
