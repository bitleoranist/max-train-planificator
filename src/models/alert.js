const mongoose = require('mongoose');

const alertSchema = new mongoose.Schema({
  fbUserId: 'string',
  trainOrigin: {
    date: 'date',
    station: 'string',
  },
  trainDestination: {
    date: 'date',
    station: 'string',
  },
});

const Alert = mongoose.model('Alert', alertSchema);

module.exports = Alert;
