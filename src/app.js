const config = require('config');
const mongoose = require('mongoose');

const logger = require('./logger');

// connect to the db
mongoose.connect(config.get('mongoose'));

module.exports = {
  config,
  logger,
};
