const request = require('request-promise');
const PQueue = require('p-queue');
const Promise = require('bluebird');
const delay = require('delay');
const he = require('he');

module.exports = (app) => {
  // request.debug = process.env.NODE_ENV === 'development';

  const { logger } = app;

  class QueueClass {
    constructor() {
      // eslint-disable-next-line no-underscore-dangle
      this._queue = [];
      this.maxCall = Math.floor(Math.random() * 25) + 45; // official limit is 50 but we're cautious, so the limit is a random bewteen 25 and 45
      this.maxTimePerSlave = 61 * 1000; // official limit is 60 but we're cautious, we add 1sec more
      this.amountCalls = 0;
      this.startCalls = new Date();
    }

    enqueue(run, options) {
      // eslint-disable-next-line no-underscore-dangle
      this._queue.push(run);
    }

    dequeue() {
      // eslint-disable-next-line no-underscore-dangle

      if (this.amountCalls >= this.maxCallPerMinute) { // if the maximum of calls is acheived
        const dateDiff = new Date().getTime() - this.startCalls.getTime(); // diff between now and the start of the measure

        if (dateDiff < this.maxTimePerSlave) { // if there is less than 1min
          const delayTime = dateDiff - this.maxTimePerSlave; // cumpute the necessary stop delay
          logger.silly(`API limit exeded, delay calls by ${delayTime}`);
          delay(delayTime); // delay the process
        }

        this.startCalls = new Date();
        this.amountCalls = 0;
      }

      this.amountCalls = this.amountCalls + 1;

      // eslint-disable-next-line no-underscore-dangle
      return this._queue.shift();
    }

    get size() {
      // eslint-disable-next-line no-underscore-dangle
      return this._queue.length;
    }
  }

  const queue = new PQueue({
    concurrency: Infinity,
    queueClass: QueueClass,
  });

  async function requestQueued(...args) {
    return new Promise((resolve, refuse) => {
      queue.add(() => {
        request(...args)
          .then(resolve)
          .catch(refuse);
      });
    });
  }

  function getSncfApiFormattedDate(date) {
    return date.toISOString().slice(0, -5);
  }

  async function tgvMaxSimulatorApiToken() {
    logger.debug('Get API token');
    const res = await requestQueued({
      headers: {
        'accept-charset': 'utf-8',
      },
      url: 'https://simulateur.tgvmax.fr/trainline',
    });

    const tokenResearch = /<input[^><]*name="hiddenToken"[^><]*value="([\w=&#;/]+)"[^><]*\/>/i;
    const researchResponse = tokenResearch.exec(res);

    if (!(researchResponse && researchResponse.length === 2)) {
      return new Error('JSON token has not been detected in the HTML response');
    }

    const token = he.decode(researchResponse[1]); // "he" is here to decode HTM entities
    return token;
  }

  async function tgvMaxSimulatorApiRailAvailabilityFromDate(reqToken, reqStart) {
    logger.debug(`Get SNCF availabilities from ${reqStart.toJSON()} to infinity`);

    const infinity = new Date('2999-01-01');
    const url = `https://sncf-simulateur-api-prod.azurewebsites.net/api/RailAvailability/Search/MARSEILLE ST CHARLES/PARIS (intramuros)/${getSncfApiFormattedDate(reqStart)}/${getSncfApiFormattedDate(infinity)}`;

    const jsonResponse = await requestQueued({
      url,
      json: true,
      headers: {
        ValidityToken: reqToken,
        Origin: 'https://simulateur.tgvmax.fr',
        Referer: 'https://simulateur.tgvmax.fr/trainline/',
      },
    });

    return jsonResponse;
  }

  async function tgvMaxSimulatorApiRailAvailabilityBetweenDates(reqToken, reqStart, reqEnd) {
    logger.debug(`Get SNCF availabilities from ${reqStart.toJSON()} to ${reqEnd.toJSON()}`);

    let availabilities = [];
    let searchBegin = reqStart;

    while (true) {
      const availability = await tgvMaxSimulatorApiRailAvailabilityFromDate(reqToken, searchBegin);
      availabilities = availabilities.concat(availability);

      const lastTimeSearched = new Date(availabilities[availabilities.length - 1].departureDateTime);
      if (lastTimeSearched.getTime() > reqEnd.getTime()) {
        break;
      }

      searchBegin = lastTimeSearched;
    }

    const availabilitiesWithFreeSeats = [];
    // delete all trains which have 0 free seats
    availabilities.forEach((entry) => {
      if (parseInt(entry.availableSeatsCount, 0) > 0) {
        availabilitiesWithFreeSeats.push(entry);
      }
    });

    return availabilitiesWithFreeSeats;
  }

  async function tgvMaxSimulatorApiOriginsAll(reqToken) {
    const jsonResponse = await requestQueued({
      url: 'https://sncf-simulateur-api-prod.azurewebsites.net/api/Stations/AllOrigins',
      json: true,
      headers: {
        ValidityToken: reqToken,
        Origin: 'https://simulateur.tgvmax.fr',
        Referer: 'https://simulateur.tgvmax.fr/trainline/',
      },
    });

    return jsonResponse;
  }

  async function tgvMaxSimulatorApiDestinationsByOrigin(reqToken, reqOrigin) {
    const jsonResponse = await requestQueued({
      url: `https://sncf-simulateur-api-prod.azurewebsites.net/api/Stations/Destinations/${reqOrigin}`,
      json: true,
      headers: {
        ValidityToken: reqToken,
        Origin: 'https://simulateur.tgvmax.fr',
        Referer: 'https://simulateur.tgvmax.fr/trainline/',
      },
    });

    return jsonResponse;
  }

  return {
    tgvMaxSimulatorApiToken,
    tgvMaxSimulatorApiRailAvailabilityFromDate,
    tgvMaxSimulatorApiRailAvailabilityBetweenDates,
    tgvMaxSimulatorApiOriginsAll,
    tgvMaxSimulatorApiDestinationsByOrigin,
  };
};
